#pragma once

#define PAUSE printf("\n"); system("pause")
#define CLEAR system("cls")	
#define ENTER printf("\n")
#define TAB printf("\t")

typedef enum codeErreur CodeErreur;
enum codeErreur {
	PAS_D_ERREUR,
	OUVERTURE_FICHIER,
	ALLOCATION_MEMOIRE
};